const fs = require("fs").promises; //Imported fs module
const path = require("path"); //Imported path module


function boardInfo(boardId){
    const boardFilePath = path.resolve(__dirname, "boards_1.json");
    return new Promise((resolve,reject) =>{
        setTimeout(() => {
            fs.readFile(boardFilePath, "utf-8")
               .then((data) => {
                   let boardData = JSON.parse(data);
                   let particularBoard = boardData.find(board => board.id === boardId);
                   // console.log("ji");
                   return resolve(particularBoard);
               })
               .catch((err) => {
                   reject(err);
               })
       },2*1000)
    })
    
}


module.exports = boardInfo;















