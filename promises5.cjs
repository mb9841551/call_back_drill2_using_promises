const boardInfo = require("./promises1.cjs");
const getAllLists = require("./promises2.cjs");
const getAllCards = require("./promises3.cjs");

// Function to get information from Thanos boards, all lists, and all cards for the Mind list
function getInfoAndCardFromMindAndSpace() {
    let thanosId;
    let thanosLists;
    // let mindListId;

    return new Promise((resolve,reject) => {
        boardInfo("mcu453ed")
        .then((board) => {
            if (!board) {
                reject("Thanos board not found");
            }

            thanosId = board.id;
            // console.log("Thanos board details:", board);

            return getAllLists(thanosId);
        })
        .then((lists) => {
            if (!lists) {
                reject("Lists for Thanos not found");
            }

            thanosLists = lists;
            // console.log("Thanos lists details:", thanosLists);

            const mindList = lists.filter((list) => list.name === "Mind" || list.name === "Space");
            const mindListId  = mindList.map((list) => list.id);
            // console.log(mindListId);

            if (!mindList) {
                reject("Mind list not found in Thanos board");
                return;
            }

            // mindListId = mindList.id;
            const promises = mindListId.map((powerList) => getAllCards(powerList))
            // return getAllCards(mindListId);
            return Promise.all(promises)
        })
        .then((mindAndSpaceCards) => {
            if (!mindAndSpaceCards) {
                reject("Cards for Mind and Space list not found");
            }

            // console.log("Mind list cards details:", cards);

            // Returning the relevant information
            resolve( {
                thanosBoard: thanosId,
                thanosLists: thanosLists,
                mindAndSpaceListCards: mindAndSpaceCards.flat(),
        });
        })
        .catch((err) => {
            reject(err);
            // throw err; // Re-throw the error to maintain the error chain
        });
    })
        
}


module.exports = getInfoAndCardFromMindAndSpace;

