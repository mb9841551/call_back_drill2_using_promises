const boardInfo = require("./promises1.cjs");
const getAllLists = require("./promises2.cjs");
const getAllCards = require("./promises3.cjs");


function getAllInfoFromThanos() {
    let thanosId;
    let thanosLists;

    return new Promise((resolve,reject) => {
        boardInfo("mcu453ed")
        .then((board) => {
            if (!board) {
                reject("Thanos board not found");
            }

            thanosId = board.id;

            return getAllLists(thanosId);
        })
        .then((lists) => {
            if (!lists) {
                reject("Lists for Thanos not found");
            }

            thanosLists = lists;

            // Use Promise.all to fetch cards for all lists simultaneously
            const promises = lists.map((list) => getAllCards(list.id));
            return Promise.all(promises);
        })
        .then((allCards) => {
            if (!allCards) {
                reject("Cards for lists not found");
            }

            // Flatten the array and log the content of each card object
            const flattenedCards = allCards.flat();
            // flattenedCards.forEach((card, index) => {
            //     console.log(card);
            // });

            resolve({
                thanosBoard: thanosId,
                thanosLists: thanosLists,
                allListsCards: flattenedCards,
            });
            
        })
        .catch((err) => {
            console.error(err);
        });
    })
    
}

module.exports = getAllInfoFromThanos;

