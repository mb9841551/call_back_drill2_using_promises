const boardInfo = require("./promises1.cjs");
const getAllLists = require("./promises2.cjs");
const getAllCards = require("./promises3.cjs");

// Function to get information from Thanos boards, all lists, and all cards for the Mind list
function getInfoAndCardFromMind() {
    let thanosId;
    let thanosLists;
    let mindListId;

    return new Promise((resolve,reject) => {
        boardInfo("mcu453ed")
        .then((board) => {
            if (!board) {
                reject("Thanos board not found");
            }

            thanosId = board.id;
            // console.log("Thanos board details:", board);

            return getAllLists(thanosId);
        })
        .then((lists) => {
            if (!lists) {
                reject("Lists for Thanos not found");
            }

            thanosLists = lists;
            // console.log("Thanos lists details:", thanosLists);

            const mindList = lists.find((list) => list.name === "Mind");

            if (!mindList) {
                reject("Mind list not found in Thanos board");
                return;
            }

            mindListId = mindList.id;

            return getAllCards(mindListId);
        })
        .then((cards) => {
            if (!cards) {
                reject("Cards for Mind list not found");
            }

            // console.log("Mind list cards details:", cards);

            // Returning the relevant information
            resolve( {
                thanosBoard: thanosId,
                thanosLists: thanosLists,
                mindListCards: cards
        });
        })
        .catch((err) => {
            reject(err);
            // throw err; // Re-throw the error to maintain the error chain
        });
    })
        
}


module.exports = getInfoAndCardFromMind;

