const fs = require("fs").promises; //Imported fs module
const path = require("path"); //Imported path module

function getAllLists(boardId){
    const listFilePath = path.join(__dirname,"lists_1.json");
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            fs.readFile(listFilePath , "utf-8")
            .then((data) => {
                const listData = JSON.parse(data);
                const particularList = listData[boardId];
                return resolve(particularList);
            })
            .catch((err) => {
                console.error("Error in getting list data",err);
            })
        },2*1000)
    })
}

module.exports = getAllLists; //Exported the getAllLists function










