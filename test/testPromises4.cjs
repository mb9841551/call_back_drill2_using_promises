const getInfoAndCardFromMind = require("../promises4.cjs");

// Using the function
getInfoAndCardFromMind()
    .then((result) => {
        console.log("Result:", result);
    })
    .catch((err) => {
        console.error("Error:", err);
    });