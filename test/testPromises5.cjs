const getInfoAndCardFromMindAndSpace = require("../promises5.cjs");

// Using the function
getInfoAndCardFromMindAndSpace()
    .then((result) => {
        console.log("Result:", result);
    })
    .catch((err) => {
        console.error("Error:", err);
    });