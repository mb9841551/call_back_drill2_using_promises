const getAllInfoFromThanos = require("../promises6.cjs");

// Using the function
getAllInfoFromThanos()
.then((result) => {
    console.log("Result:", result);
})
.catch((err) => {
    console.error("Error:", err);
});
